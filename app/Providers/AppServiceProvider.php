<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Cache;
use App\Models\Setting;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $settings = Cache::rememberForever('settings', function () {
            $items = Setting::get()->toArray();
            $settings = [];
            if ($items !== null) {
                foreach ($items as $k => $v) {
                    $settings[$v['name']] = is_array(json_decode($v['value'], true)) ? json_decode($v['value'], true) : $v['value'];
                }
            }
            return $settings;
        });
        config()->set('settings', $settings);
    }
}