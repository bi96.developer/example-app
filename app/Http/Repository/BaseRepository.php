<?php

namespace App\Repository;

use App\Http\Service\CommonService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class BaseRepository
{
    /**      
     * @var Model      
     */
    protected $model;

    protected $_lang;

    public function __construct(CommonService $commonService)
    {
        $this->commonService = $commonService;
        $this->thisName = $this->commonService->getClassName($this, true);
        $this->model = 'App\Models\\' . $this->thisName;

        $this->_lang = App::getLocale();
    }

    public function create(array $data): Model
    {
        return $this->model::create($data);
    }

    public function update(array $data)
    {
        return $this->model->save($data);
    }

    public function all()
    {
        return $this->model->all();
    }

    public function find($id): ?Model
    {
        return $this->model->find($id);
    }
}