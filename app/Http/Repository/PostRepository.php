<?php

namespace App\Repository;

use App\Models\Post;
use Illuminate\Support\Facades\DB;

class PostRepository extends BaseRepository
{

    public function getListPostAdmin($request)
    {

        $whereRaw = $this->buildConditions($request);

        $items = DB::table('posts as p')
            ->leftJoin('post_languages as pl', 'p.id', '=', 'pl.post_id')
            ->whereRaw($whereRaw)
            ->orderBy('p.priority', 'desc')
            ->orderBy('p.id', 'desc')
            ->limit(25)
            ->get();

        return $items;
    }

    public function buildConditions($request)
    {
        $whereRaw = "p.type='" . $request['type'] . "'";
        $whereRaw .= $request->title ? " and (pl.title like '%" . $request->title . "%') " : "";
        $whereRaw .= $request->created_at ? " and p.created_at like '%" . $request->created_at . "%'" : "";

        return $whereRaw;
    }

    public function getListLastedNews()
    {

        $news = DB::table('posts as p')
            ->leftJoin('post_languages as pl', 'p.id', '=', 'pl.post_id')
            ->where('pl.locale', $this->_lang)
            ->where('p.status', Post::STATUS_ACTIVE)
            ->orderBy('p.priority', 'desc')
            ->orderBy('p.id', 'desc')
            ->limit(10)
            ->get();

        return $news;
    }
}