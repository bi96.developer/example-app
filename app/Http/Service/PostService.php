<?php

namespace App\Http\Service;

use App\Models\Post;
use App\Models\PostLanguage;
use App\Repository\PostRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PostService extends BaseService
{
    private $repository;

    public function __construct(Request $request, PostRepository $postRepository)
    {
        $this->repository = $postRepository;
        parent::__construct($request, config('siteConfigs.post'));
    }

    public function getListPostAdmin($request)
    {
        $this->_data['items'] = $this->repository->getListPostAdmin($request);
        return $this->_data;
    }

    public function add($request)
    {

        $valid = Validator::make($request->all(), [
            'dataL.vi.title'   => 'required',
            'image'            => 'image|max:2048'
        ], [
            'dataL.vi.title.required'   => 'Vui lòng nhập Tên Bài Viết',
            'image.image'               => 'Không đúng chuẩn hình ảnh cho phép',
            'image.max'                 => 'Dung lượng vượt quá giới hạn cho phép là :max KB'
        ]);

        if ($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        } else {

            if ($request->data) {
                foreach ($request->data as $field => $value) {
                    $data[$field] = $value;
                }
            }

            $data['priority'] = (int)str_replace('.', '', $request->priority);
            $data['status']   = ($request->status) ? $request->status : '';
            $data['type']     = $this->_data['type'];

            $page = $this->repository->create($data);

            // Save data Lang
            $dataL = [];
            $dataInsert = [];
            foreach ($this->_data['languages'] as $lang => $val) {

                if ($request->dataL[$lang]) {
                    foreach ($request->dataL[$lang] as $fieldL => $valueL) {
                        $dataL[$fieldL] = $valueL;
                    }
                }

                if (!isset($request->dataL[$this->_data['default_language']]['slug']) || $request->dataL[$this->_data['default_language']]['slug'] == '') {
                    $dataL['slug'] = Str::slug($request->dataL[$this->_data['default_language']]['title']);
                } else {
                    $dataL['slug'] = Str::slug($request->dataL[$this->_data['default_language']]['slug']);
                }
                $dataL['locale']   = $lang;
                $dataInsert[]      = new PostLanguage($dataL);
            }

            $page->languages()->saveMany($dataInsert);
        }
        return $this->_data;
    }
}