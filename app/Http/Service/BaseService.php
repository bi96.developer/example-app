<?php

namespace App\Http\Service;

class BaseService
{
    protected $_data;

    public function __construct($request, $siteConfigs)
    {
        $this->_data['type'] = (isset($request->type) && $request->type != '') ? $request->type : 'default';
        $this->_data['site_configs'] = $siteConfigs;
        $this->_data['default_language'] = config('siteConfigs.general.language');
        $this->_data['languages'] = config('siteConfigs.languages');
        $this->_data['path'] = $this->_data['site_configs']['path'];
        $this->_data['thumbs'] = $this->_data['site_configs'][$this->_data['type']]['thumbs'];
        $this->_data['pageTitle'] = $this->_data['site_configs'][$this->_data['type']]['page-title'];
    }
}