<?php

namespace App\Http\Service;

class CommonService
{

    public function __construct()
    {
    }

    public function getClassName($object, $noTypeText = false)
    {
        $className = str_replace('Proxies\\__CG__\\', '', is_string($object) ? $object : get_class($object));
        $parts = explode('\\', $className);
        $serviceName = $parts[count($parts) - 1];
        if ($noTypeText) {
            return str_replace(
                ['Service', 'Repository', 'Models', 'Controller'],
                ['', '', '', ''],
                $serviceName
            );
        }
        return $serviceName;
    }
}