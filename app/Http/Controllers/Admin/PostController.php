<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Service\PostService;

class PostController extends Controller
{

    public function index(Request $request, PostService $postService)
    {

        $data = $postService->getListPostAdmin($request);
        return view('admin.post.index', [
            'data' => $data
        ]);
    }

    public function create(Request $request, PostService $postService)
    {
        $data = [];
        return redirect()->route('admin.post.create', ['data' => $data]);
    }

    public function store(Request $request, PostService $postService)
    {
        $result = $postService->add($request);
        return redirect()->route('admin.post.index', ['type' => $result['type']])
            ->with('success', 'Thêm dữ liệu thành công');
    }
}