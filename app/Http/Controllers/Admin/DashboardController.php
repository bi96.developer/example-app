<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AbstractController;

class DashboardController extends AbstractController
{
    public function index()
    {
        return view('admin.layouts.dashboard');
    }
}