<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

abstract class AbstractController extends Controller
{
    protected $_data;
    protected $_configs;

    public function __construct()
    {
        //dd($siteConfig);
        // $this->_data['type'] = (isset($request->type) && $request->type != '') ? $request->type : 'default';
        $this->_data['siteconfig'] = $this->_configs;
        $this->_data['default_language'] = config('siteconfig.general.language');
        $this->_data['languages'] = config('siteconfig.languages');
        $this->_data['path'] = $this->_data['siteconfig']['path'];
        //$this->_data['thumbs'] = $this->_data['siteconfig'][$this->_data['type']]['thumbs'];
        //$this->_data['pageTitle'] = $this->_data['siteconfig'][$this->_data['type']]['page-title'];
        dd($this->_data['siteconfig']);
    }

    protected function setConfigs($configs)
    {
        dd($configs);
        $this->_configs = $configs;
    }
}