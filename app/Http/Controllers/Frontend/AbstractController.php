<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use romanzipp\Seo\Facades\Seo;
use romanzipp\Seo\Services\SeoService;
use Illuminate\Support\Facades\App;

abstract class AbstractController extends Controller
{
    protected $_data;
    protected $_lang;

    public function __construct()
    {
        $this->_lang = (session('lang')) ? session('lang') : config('settings.language');
        App::setLocale($this->_lang);

        $this->seo = new Seo();
        $this->seo = app(SeoService::class);
        $this->seo = Seo::make();
    }

    public function setTitle($title)
    {
        $this->seo->title($title);
    }

    public function setDescription($description)
    {
        $this->seo->description($description);
    }

    public function addMeta($name, $content)
    {
        $this->seo->meta($name, $content);
    }
}