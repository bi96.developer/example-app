<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repository\PostRepository;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(PostRepository $postRepository)
    {
        $posts = $postRepository->all();
        return view('frontend.layouts.home', $posts);
    }

    public function detail(REQUEST $request)
    {
        $data = [];
        return view('frontend.layouts.home', $data);
    }
}