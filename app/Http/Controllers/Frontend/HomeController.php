<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Frontend\AbstractController;
use App\Repository\PostRepository;

class HomeController extends AbstractController
{

    public function index(PostRepository $postRepository)
    {
        $this->setTitle('Home page');
        $this->setDescription('Home page');

        $this->_data['posts'] = $postRepository->getListLastedNews();

        return view('frontend.layouts.home', [
            'data' => $this->_data
        ]);
    }
}