<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Setting extends Model
{
	use HasFactory;
	protected $table = 'settings';
	protected $guarded = [];
	public $timestamps = false;

	public static function boot()
	{
		parent::boot();
		static::deleted(function ($setting) {
			Cache::forget("settings");
		});
		static::created(function ($setting) {
			Cache::forget("settings");
		});
		static::updated(function ($setting) {
			Cache::forget("settings");
		});
	}
}