<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    protected $table = 'posts';
    protected $guarded = [];


    public function languages()
    {
        return $this->hasMany(PostLanguage::class, 'post_id', 'id')->orderBy('id', 'asc');
    }
}