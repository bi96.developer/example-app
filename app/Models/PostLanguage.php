<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostLanguage extends Model
{
    use HasFactory;

    protected $table = 'post_languages';
    protected $guarded = [];
    public $timestamps = false;

    public function post()
    {
        return $this->belongsTo('App\Post', 'post_id');
    }
}