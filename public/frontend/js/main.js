(function($) {
    "use strict";
    var MEDIA = {};

    /*************************
        Predefined Variables
        *************************/
    var $window = $(window),
        $document = $(document);

    //Check if function exists
    $.fn.exists = function() {
        return this.length > 0;
    };

    MEDIA.menuMobile = function() {
        /*Variables*/
        var $offCanvasNav = $(".off-canvas-nav"),
            $offCanvasNavSubMenu = $offCanvasNav.find(".sub-menu");

        /*Add Toggle Button With Off Canvas Sub Menu*/
        $offCanvasNavSubMenu
            .parent()
            .prepend('<span class="menu-expand"><i></i></span>');

        /*Close Off Canvas Sub Menu*/
        $offCanvasNavSubMenu.slideUp();

        /*Category Sub Menu Toggle*/
        $offCanvasNav.on("click", "li a, li .menu-expand", function(e) {
            var $this = $(this);
            if (
                $this
                .parent()
                .attr("class")
                .match(/\b(menu-item-has-children|has-children|has-sub-menu)\b/) &&
                ($this.attr("href") === "#" || $this.hasClass("menu-expand"))
            ) {
                e.preventDefault();
                if ($this.siblings("ul:visible").length) {
                    $this.parent("li").removeClass("active");
                    $this.siblings("ul").slideUp();
                } else {
                    $this.parent("li").addClass("active");
                    $this.closest("li").siblings("li").find("ul:visible").slideUp();
                    $this.siblings("ul").slideDown();
                }
            }
        });

        // Off Canvas Open close
        $(".off-canvas-btn").on("click", function(e) {
            e.preventDefault();
            $(".off-canvas-wrapper").addClass("open");
            $(".off-canvas-overlay").addClass("open");
        });

        $(".btn-close-off-canvas, .off-canvas-overlay").on("click", function() {
            $(".off-canvas-wrapper").removeClass("open");
            $(".off-canvas-overlay").removeClass("open");
        });
    };

    MEDIA.slickSlider = function() {
        var slickSlider = $(".slick-slider");
        slickSlider.each(function() {
            var slickItem = $(this).data("item") || 1;
            var slickArrow = $(this).data("arrows");
            var slickFade = $(this).data("fade") || false;
            var slickSpeed = $(this).data("speed") || 600;
            var slickAutoplaySpeed = $(this).data("autoplayspeed") || 3000;
            var slickAutoplay = $(this).data("autoplay") === false ? false : true;
            $(this).slick({
                infinite: true,
                slidesToShow: slickItem,
                slidesToScroll: 1,
                autoplay: slickAutoplay,
                dots: true,
                arrows: slickArrow,
                centerPadding: "30",
                autoplaySpeed: slickAutoplaySpeed,
                speed: slickSpeed,
                fade: slickFade,
                pauseOnHover: false,
                pauseOnFocus: false,
                responsive: [{
                        breakpoint: 1800,
                        settings: {
                            slidesToShow: slickItem,
                            centerPadding: "30",
                        },
                    },
                    {
                        breakpoint: 1350,
                        settings: {
                            slidesToShow: slickItem,
                            centerPadding: "0",
                        },
                    },
                    {
                        breakpoint: 1250,
                        settings: {
                            slidesToShow: slickItem,
                            centerPadding: "0px",
                        },
                    },
                    {
                        breakpoint: 1150,
                        settings: {
                            slidesToShow: slickItem,
                            centerPadding: "0px",
                        },
                    },
                    {
                        breakpoint: 1050,
                        settings: {
                            slidesToShow: slickItem,
                            centerPadding: "0",
                        },
                    },
                    {
                        breakpoint: 950,
                        settings: {
                            slidesToShow: slickItem,
                            centerPadding: "0",
                        },
                    },
                    {
                        breakpoint: 850,
                        settings: {
                            slidesToShow: slickItem,
                            centerPadding: "0",
                        },
                    },
                    {
                        breakpoint: 515,
                        settings: {
                            slidesToShow: 1,
                            autoplay: slickAutoplay,
                            centerPadding: "0px",
                        },
                    },
                ]
            });
        });
    };

    MEDIA.scrollToTop = function() {
        var scrollTopBtn = $(".scroll-top");
        $(window).scroll(function() {
            if ($(this).scrollTop() > 200) {
                $(scrollTopBtn).fadeIn();
            } else {
                $(scrollTopBtn).fadeOut();
            }
        });

        $(scrollTopBtn).on("click", function() {
            $("html, body").animate({ scrollTop: 0 }, 1000);
        });
    };

    //Document ready functions
    $document.ready(function() {
        AOS.init({
            once: true,
            offset: 150,
        });
        $(".preloader").fadeOut(1000, function() {
            AOS.refresh();
        });
        MEDIA.menuMobile();
        MEDIA.slickSlider();
        MEDIA.scrollToTop();

        $(document).on('click', ".lines-button", function () {
            $('body').css('overflow','hidden');
            $('.lines-button').addClass('active');
            $('.navbar-brand').addClass('control-logo');
            $('html, body').animate({ scrollTop: 0 }, 100);
        });
        $(document).on('click', ".lines-button.active", function () {
            $('body').css('overflow','auto');
            $('.lines-button').removeClass('active');
            $('.navbar-brand').removeClass('control-logo');
        });
    });
})(jQuery);
window.addEventListener('resize', () => {
    // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
    let vh = window.innerHeight * 0.01; console.log('in',vh);
    // Then we set the value in the --vh custom property to the root of the document
    document.documentElement.style.setProperty('--vh', `${vh}px`);
});

// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01; console.log('out',vh);
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);