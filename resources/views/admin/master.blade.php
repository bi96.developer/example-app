<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />

    {{ seo()->render() }}

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
    @yield('custom_css')
</head>

<body class="d-flex flex-column min-vh-100">

    @include('frontend.layouts._header')

    <main class="flex-grow-1 mb-lg-5 mb-4">
        <div class="container">
            @yield('content')
        </div>
    </main>

    @include('frontend.layouts._footer')

    <script src="{{ asset('frontend/js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('frontend/js/aos-animation/aos.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap/popper.min.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend/js/slick/slick.min.js') }}"></script>
    <script src="{{ asset('frontend/js/main.js') }}"></script>

    @yield('custom_script')

</body>

</html>