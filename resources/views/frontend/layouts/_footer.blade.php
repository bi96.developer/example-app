<footer>
    <div class="f-infor py-4">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-2 f-logo text-lg-left text-center mb-lg-0 mb-4">
                    <a href="{{ route('frontend.home.index') }}">
                        <img src="{{ asset('frontend/images/sa-logo.svg') }}" alt="" />
                        <span class="d-block text-white text-center">NTD Dev</span>
                    </a>
                </div>
                <div class="col-lg-10">
                    <div class="row">
                        <div class="col-md-4 mb-lg-0 mb-3">
                            <div class="f-box location h-100 d-flex flex-row align-items-center">
                                <i class="fa-solid fa-location-dot mr-4"></i>
                                <span>
                                    <strong>Adress</strong>
                                    19 Boulevard des Nations Unies<br />
                                    92190 Meudon
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 mb-lg-0 mb-3">
                            <div class="f-box phone h-100 d-flex flex-row align-items-center">
                                <i class="fa-solid fa-phone mr-4"></i>
                                <span>
                                    <strong>Phone</strong>
                                    06 44 15 92 74
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-box email h-100 d-flex flex-row align-items-center">
                                <i class="fa-solid fa-envelope mr-4"></i>
                                <span>
                                    <strong>Email</strong>
                                    <a href="mailto:ntdoan.dev">ntdoan.dev</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="f-copyright text-center">
        Copyright © 2022 NTD dev
    </div>
</footer>

<div class="scroll-top">
    <div class="scroll-top__inner">
        <i class="far fa-chevron-up"></i>
    </div>
</div>