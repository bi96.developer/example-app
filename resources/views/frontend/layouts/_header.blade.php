<header class="py-0 mb-3">
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-expand-md justify-content-between w-100">
                <!-- Brand -->
                <a class="navbar-brand py-0" href="{{ route('frontend.home.index') }}">
                    <img src="{{ asset('frontend/images/sa-logo.svg') }}" alt="" />
                    <span class="d-block text-white text-center">NTD Dev</span>
                </a>

                <button class="navbar-toggler lines-button lines" type="button" data-toggle="collapse"
                    data-target="#MediaNavbar"><span></span></button>
                <!-- Links -->
                <div class="collapse navbar-collapse justify-content-end" id="MediaNavbar">
                    <ul class="navbar-nav align-items-center mb-0">
                        <li class="nav-item">
                            <a class="nav-link active" href="{{ route('frontend.home.index') }}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('frontend.page.about') }}">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('frontend.news.index') }}">News</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('frontend.contact.index') }}">Actualites</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>