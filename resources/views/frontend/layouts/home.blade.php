@extends('frontend.master')
@section('content')
<div class="row">
    <div class="col-12 d-lg-flex d-block news-container">
        <div class="col-content">
            <div class="news-three">
                <div class="row">
                    @foreach ($data['posts'] as $post)
                    <div class="col-md-3 mb-3">
                        <div class="n-item d-block align-items-center">
                            <div class="n-img mb-2">
                                <a href="{{ $post->slug }}">
                                    <img src="{{ asset('frontend/images/thumb/a1.jpg') }}" alt="" />
                                </a>
                            </div>
                            <h2 class="w-sb mb-md-2 mb-0">
                                <a href="{{ $post->slug }}">{{ $post->title }}</a>
                            </h2>
                            <div class="public mb-2">
                                <i class="fa-regular fa-clock mr-1"></i>
                                <span>
                                    Publication le {!! date('d-m-Y', strtotime($post->created_at)) !!}
                                </span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection