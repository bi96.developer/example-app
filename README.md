<p align="center"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></p>

## Setup project Larevel

```
php composer install
```

## Run migrate

```
php artisan migrate
```

## Create Fake database

```
php artisan db:seed
```
