<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\PostLanguage;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::factory(10)->create()->each(function ($p) {
            foreach (['vi', 'en'] as $lang) {
                $p->languages()->save(PostLanguage::factory()->make(['locale' => $lang]));
            }
        });
    }
}