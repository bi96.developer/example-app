<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostLanguageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name                    =  $this->faker->name;
        $meta_seo['title']       = $name;
        $meta_seo['keywords']    = $name;
        $meta_seo['description'] = $name;

        return [
            'title'                 => $name,
            'slug'                  => Str::slug($name),
            'description'           => $this->faker->text,
            'contents'              => $this->faker->paragraph,
            'meta_title'            => $meta_seo['title'],
            'meta_keywords'         => $meta_seo['keywords'],
            'meta_description'      => $meta_seo['description'],
            'locale'              => 'vi'
        ];
    }
}