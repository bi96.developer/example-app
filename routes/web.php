<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\PostController as AdminPostController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\PageController;
use App\Http\Controllers\Frontend\PostController;
use App\Http\Controllers\Frontend\ContactController;
use Illuminate\Support\Facades\Route;



Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin'], function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard.index');

    Route::get('/posts', [AdminPostController::class, 'index'])->name('post.index');
    Route::get('/posts/create', [AdminPostController::class, 'create'])->name('post.create');
    Route::post('/posts', [AdminPostController::class, 'store'])->name('post.store');
});


Route::group(['as' => 'frontend.', 'namespace' => 'Frontend'], function () {

    Route::get('/', [HomeController::class, 'index'])->name('home.index');

    Route::get('/about', [PageController::class, 'index'])->name('page.about');

    Route::get('/news', [PostController::class, 'index'])->name('news.index');
    Route::get('/{slug}', [PostController::class, 'detail'])->name('news.detail');

    Route::get('/contact', [ContactController::class, 'index'])->name('contact.index');
});